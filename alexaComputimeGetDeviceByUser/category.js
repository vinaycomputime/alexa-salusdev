//TEST NUMBER 6
//DO NOT use 'async' function for normal operation 
function IsIT600Thermostat(devType)
{
    if(devType == 'it600ThermHW' || devType == 'SQ610RF' || devType == 'SQ610'){
        return true;
    }
    else{
        return false;
    }
}

function IsSmartPlug(devType)
{
    if(devType == 'SP600' || devType == 'SPE600'){
        return true;
    }
    else{
        return false;
    }
}

function IsSoloThermostat(devType)
{
    if(devType == 'ST920WF' || devType == 'ST921WF'){
        return true;
    }
    else{
        return false;
    }
}

function IsSmartLight(devType)
{
    if(devType == 'PAR16_RGBW_Z3' || devType == 'FLEX_RGBW_Z3' || devType == 'PAR16_DIM'){
        return true;
    }
    else{
        return false;
    }
}

function IsSensor(devType)
{
    if(devType == 'MS600'){
        return true;
    }
    else{
        return false;
    }
}

module.exports = {IsSoloThermostat, IsIT600Thermostat, IsSmartPlug, IsSmartLight, IsSensor};
