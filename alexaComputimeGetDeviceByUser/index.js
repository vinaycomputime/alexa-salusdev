/* Use this device ID for testing devlistGW = ["SAU2AG1_GW-001E5E0082CA"]; */

const AWS = require("aws-sdk");
const dynamodb = new AWS.DynamoDB.DocumentClient();

const DEVICE_TABLE_NAME = process.env.DEVICE_TABLE;
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
const IOT_REGION = process.env.REGION;
const categoryFxn = require("./category");
//Variable to test the functionality with hardcoded values
var global_debug = 0;

exports.handler = async (event, context) => {

    try {
        console.log(`Received event:\n${JSON.stringify(event)}`);
        var deviceList = await getDevicesByUser(event.userId);
        const response = {
            deviceList: deviceList
        };
        return response;
    } catch (error) {
        console.log(`Error:\n${error}`);
        throw error; 
    }
};

async function getDevicesByUser(userId) {

    // First, get the IoT Thing Names associated to our user.
    var params = {
        ExpressionAttributeValues: {
            ":userid": userId
        }, 
        ProjectionAttributes: "userid",
        KeyConditionExpression: "userid = :userid", 
        TableName: DEVICE_TABLE_NAME
    };
    
    var query_response = await dynamodb.query(params).promise();
    
    var ownlist = (JSON.parse(query_response.Items[0].Own));
    var sharelist = (JSON.parse(query_response.Items[0].Own));
    var devlistGW = ownlist.list;

    if(global_debug == 1)
    devlistGW = ["SAU2AG1_GW-001E5E0082CA"];
    else
    devlistGW.concat(sharelist.list);

    console.log('devlistGW: ' + devlistGW);

    var userDeviceAssociations = [];
    var devType;
    var devlist;
    var idx;
    var IsSupported;
    var modeldescr;

    //SMART PLUG: extract things under gateway
    for (idx in devlistGW) {
    console.log('Gateway index...' + idx);
    devlist = await getDeviceListGateway(devlistGW);
    //devlist.forEach(dev =>  {
    //Use 'for' loop for async await fxn calls inside
    for(var dev of devlist) {
        console.log(dev);
        //20220125:
        devType = dev.split('-');
        if(devType.length > 2)
        devType = devType[2];     //not applicable for solo thermostat
        else
        devType = devType[0];     //for solo thermostat
        
        var devitem = {
            thingName: dev,
            thingType: devType,
            userId: userId
        };
        
        IsSupported = categoryFxn.IsSoloThermostat(devType);
        if(IsSupported == true)
        {
            userDeviceAssociations.push(devitem);
        }
        
        IsSupported = categoryFxn.IsIT600Thermostat(devType);
        if(IsSupported == true)
        {
            userDeviceAssociations.push(devitem);
        }

        IsSupported = categoryFxn.IsSmartPlug(devType);
        if(IsSupported == true)
        {
            userDeviceAssociations.push(devitem);
        }
        
        IsSupported = categoryFxn.IsSmartLight(devType);
        if(IsSupported == true)
        {
            userDeviceAssociations.push(devitem);
        }
        
        IsSupported = categoryFxn.IsSensor(devType);
        if(IsSupported == true)
        {
            userDeviceAssociations.push(devitem);
        }

    }
    //})
    }
    console.log(userDeviceAssociations);

    console.log('User devices associations:\n' + JSON.stringify(userDeviceAssociations, null, 2));

    // Each device name has the device type as part of the name. We parse out
    // a list of unique device types from the list of device names into an 
    // object map, below. Initially, each key in the object is a deviceType
    // equal to {}. By having a unique list of device types, we can do a single
    // BatchGetItem() call to DDB to get all of the device type metadata at once.
    // If we instead looped through each device once at a time to call DDB
    // for the corresponding metadata, we would instead make multiple calls
    // and potentially call to get the same metadata twice. 
    var deviceTypeList = getDeviceTypeListFromUserDeviceAssociations(userDeviceAssociations);

    // Now that we have the list of device types, we will query DDB for each
    // device type's metadata and then use that to populate our object map:
    var deviceTypeMetadata = await getDeviceTypeMetadata(deviceTypeList);

    // Combine the device associations with the metadata to get our final device list:
    var i;
    for (i in userDeviceAssociations) {
        var deviceType = userDeviceAssociations[i].thingType;
        Object.assign(userDeviceAssociations[i], deviceTypeMetadata[deviceType]);
        
    }

    return userDeviceAssociations;
}

function getDeviceTypeListFromUserDeviceAssociations(userDeviceAssociations) {

    var deviceTypeSet = new Set();
    userDeviceAssociations.forEach(userDeviceAssociation => {
        console.log(userDeviceAssociation)
        deviceTypeSet.add(userDeviceAssociation.thingType);
    });
    return Array.from(deviceTypeSet);
}

//SMART PLUG: new function to extract lists from gateway
async function getDeviceListGateway(devlist)
{
    console.log('getDeviceListGateway......')
    AWS.config.region = IOT_REGION;    //IoT things exist only in 'us-west-2' region
    var iot = new AWS.Iot();
    
    //20220124: Check for Gateway
    var devType = devlist[0].split('-');
    devType = devType[0].split('_');
    console.log(devType[0] + ' ' + devType[1]);    
    if(devType[1] != 'GW')
    return devlist;
    
  	var thngGrp = 'Gateway-' + devlist[0].split('-')[1];    //For one Gateway
    var paramsThing = {
	   "thingGroupName": thngGrp,
	   "maxResults": 100
    };
	var devArr = await iot.listThingsInThingGroup(paramsThing).promise();
	devlist = devArr.things;
	console.log(devlist);
    return devlist;
}

async function getDeviceTypeMetadata(deviceTypeList) {
    
    /*
        deviceTypeList = ['type1', 'type2', ...]
    */
 
    var deviceTypeMetadata = { }

    
    if (deviceTypeList.length === 0) {
        return deviceTypeMetadata;
    } else {
        
        var requestKeys = [];

        for (index in deviceTypeList) {
            var deviceTypeName = deviceTypeList[index];
            var requestKey = {
                 ModelID: deviceTypeName //,
                 //ModelName: deviceTypeName    //20210708: ModelName not a Primary partition key in SalusDev
            }

            requestKeys.push(requestKey);
        }
        var requestItems = {};
        requestItems[MODEL_TABLE_NAME] = {
            Keys: requestKeys
        }
        var params = {
            RequestItems: requestItems
        };
        console.log('BatchGet params are:\n' + JSON.stringify(params, null, 2));
        var batchGetResponse = await dynamodb.batchGet(params).promise();
        var metadataResponses = batchGetResponse.Responses[MODEL_TABLE_NAME];

        for (var index in metadataResponses) {
            var metadata = metadataResponses[index];
            deviceTypeMetadata[metadata.ModelName] = metadata;
        }
        
        return deviceTypeMetadata;
    }
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}