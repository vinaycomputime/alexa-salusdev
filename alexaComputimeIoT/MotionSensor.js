const AWS = require("aws-sdk");
const AlexaResponse = require("./AlexaResponse");
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
const IOT_EP = process.env.IOT_ENDPOINT;
const IOT_REGION = process.env.REGION;
const dynamodb = new AWS.DynamoDB.DocumentClient();


async function handleReportState(request, context, endpoint) {
	   
    var reportstateMS = endpoint.shadow.state.reported;
    var reportpropertiesMS = reportstateMS[endpoint.modeldescription.nodeID].properties;
    var propertiesprefixMS = endpoint.modeldescription.NodeEndpoint;
    var zoneSt = reportpropertiesMS[propertiesprefixMS+ ":sIASZS:ZoneState_d"]; //Need to verify
    var devname = reportpropertiesMS[propertiesprefixMS+ ":sZDO:DeviceName"];
    console.log('Motion Sensor device name: ' + devname);

    var reportdataMS = { };
    var connectivity = 1; 
    var endpointIdMS = endpoint.endpointId;
    var tokenMS = request.directive.endpoint.scope.token;
    var correlationTokenMS = request.directive.header.correlationToken;

    var detSt;
    if(zoneSt == 0)
    detSt = "NOT_DETECTED";
    else
    detSt = "DETECTED";
    
    reportdataMS[connectivity] = "OK";
    var alexaResponseMS = new AlexaResponse(
    {
        "name": 'StateReport',
        "correlationToken": correlationTokenMS,
        "token": tokenMS,
        "endpointId": endpointIdMS
    }
    );

    // Gather current properties and add to our response
    alexaResponseMS.addContextProperty({
        namespace: "Alexa.EndpointHealth",
        name: "connectivity",
        value: {
            value: reportdataMS[connectivity]
        } 
    });
 
    alexaResponseMS.addContextProperty({
        namespace: "Alexa.MotionSensor",
        name: "detectionState",
        value: detSt 
    });
    
    return alexaResponseMS.get();
    
}

function getFriendlyName(devname) 
{
    var friendlyName = JSON.parse(devname).deviceName;
    return friendlyName;
}

module.exports = {handleReportState, getFriendlyName};
