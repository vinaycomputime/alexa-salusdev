const AWS = require("aws-sdk");
const AlexaResponse = require("./AlexaResponse");
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
const IOT_EP = process.env.IOT_ENDPOINT;
const IOT_REGION = process.env.REGION;
const dynamodb = new AWS.DynamoDB.DocumentClient();
var iotdataSend;


async function handleReportState(request, context, endpoint) {
	   
    var reportstateSP = endpoint.shadow.state.reported;
    var reportpropertiesSP = reportstateSP[endpoint.modeldescription.nodeID].properties;
    var propertiesprefixSP = endpoint.modeldescription.NodeEndpoint;
    var onlevel = reportpropertiesSP[propertiesprefixSP + ":sLevelS:OnLevel"];
    var currLvl = reportpropertiesSP[propertiesprefixSP + ":sLevelS:CurrentLevel"];
    var devname = reportpropertiesSP[propertiesprefixSP+ ":sZDO:DeviceName"];
    console.log('Smart Light device name: ' + devname);

    var reportdataSL = { };
    var pwrLvl;
    var connectivity = 1; 
    var endpointIdSP = endpoint.endpointId;
    var tokenSP = request.directive.endpoint.scope.token;
    var correlationTokenSP = request.directive.header.correlationToken;

    //Seems it cannot be over 100%, tested for 90(OK to display), >100(There was a problem error display)
    pwrLvl = currLvl; //onlevel;

    reportdataSL[connectivity] = "OK";
    var alexaResponseSP = new AlexaResponse(
    {
        "name": 'StateReport',
        "correlationToken": correlationTokenSP,
        "token": tokenSP,
        "endpointId": endpointIdSP
    }
    );

    // Gather current properties and add to our response
    alexaResponseSP.addContextProperty({
        namespace: "Alexa.EndpointHealth",
        name: "connectivity",
        value: {
            value: reportdataSL[connectivity]
        } 
    });
 
    alexaResponseSP.addContextProperty({
        namespace: "Alexa.PowerLevelController",
        name: "powerLevel",
        value: pwrLvl 
    });

    alexaResponseSP.addContextProperty({
        namespace: "Alexa.ColorController",
        name: "color",
        value: {
            "hue": 0,
            "saturation": 1,
            "brightness": 1
        }
    });
    
    return alexaResponseSP.get();
    
}

async function handlePowerLevelControl(request, context, endpoint)
{
    console.log('handlePowerLevelControl....' + request.directive);

    AWS.config.region = IOT_REGION;
    iotdataSend = new AWS.IotData({ endpoint: IOT_EP });

    var endpointId = endpoint.endpointId;
    var thingName = endpoint.thingName;
    var token = request.directive.endpoint.scope.token;
    var correlationToken = request.directive.header.correlationToken;
    var requestMethod = request.directive.header.name; 
    var payload = request.directive.payload; 
    
    var alexaResponse = new AlexaResponse(
        {
            "correlationToken": correlationToken,
            "token": token,
            "endpointId": endpointId
        }
    );

	    var nodeidSP = endpoint.modeldescription.nodeID;
	    var nodeEP = endpoint.modeldescription.NodeEndpoint;
        var reportstateSP = endpoint.shadow.state.reported;
        var reportpropertiesSP = reportstateSP[nodeidSP].properties;
        var propertiesprefixSP = nodeEP;
        var currLvl = reportpropertiesSP[propertiesprefixSP + ":sLevelS:CurrentLevel"];        
        var devname = reportpropertiesSP[propertiesprefixSP+ ":sZDO:DeviceName"];
        console.log('Smart Light device name: ' + devname);

        let desiredshadowState = {
            state: {
                desired: {
                }
            }
        };
        var desireproperties = {};
        var pwrLvl;

        if(requestMethod == 'SetPowerLevel')
        {
            pwrLvl = payload.powerLevel;
            console.log('Smart Light requested SetPowerLevel......');
    		desireproperties[propertiesprefixSP + ":sLevelS:SetOnLevel"] = pwrLvl;
    		desireproperties[propertiesprefixSP + ":sLevelS:SetCurrentLevel"] = pwrLvl;    		
            desiredshadowState.state.desired[nodeidSP] = {properties: desireproperties};

            await updateThingShadow(thingName, desiredshadowState);
            
            alexaResponse.addContextProperty({
                namespace: "Alexa.PowerLevelController",
                name: "powerLevel",
                value: pwrLvl
            });

            alexaResponse.addContextProperty({
                namespace: "Alexa.ColorController",
                name: "color",
                value: {
                    "hue": 0,
                    "saturation": 1,
                    "brightness": 1
                } 
            });
    
            return alexaResponse.get();
        }
        else if(requestMethod == 'AdjustPowerLevel')
        {
            var pwrLvlDelta = payload.powerLevelDelta;
            pwrLvl = (currLvl + pwrLvlDelta);
            console.log('Smart Light requested SetPowerLevel......');
    		desireproperties[propertiesprefixSP + ":sLevelS:SetCurrentLevel"] = pwrLvl;
            desiredshadowState.state.desired[nodeidSP] = {properties: desireproperties};

            await updateThingShadow(thingName, desiredshadowState);
            
            alexaResponse.addContextProperty({
                namespace: "Alexa.PowerLevelController",
                name: "powerLevel",
                value: pwrLvl
            });

            alexaResponse.addContextProperty({
                namespace: "Alexa.ColorController",
                name: "color",
                value: {
                    "hue": 0,
                    "saturation": 1,
                    "brightness": 1
                } 
            });
    
            return alexaResponse.get();
        }
}

async function updateThingShadow(thingName, shadowState) {
    try {
        var payloadjson = JSON.stringify(shadowState)
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        // Updating shadow updates our *control plane*, but it doesn't necessarily
        // mean our device state has changed. The device is responsible for monitoring
        // the device shadow and responding to changes in desired states. 
        const request = iotdataSend.publish(params);
        var updateShadowResponse = await request.promise();
    }
    catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

function getFriendlyName(devname) 
{
    var friendlyName = JSON.parse(devname).deviceName;
    return friendlyName;
}

module.exports = {handleReportState, handlePowerLevelControl, getFriendlyName};
