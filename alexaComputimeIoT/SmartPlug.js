const AWS = require("aws-sdk");
const AlexaResponse = require("./AlexaResponse");
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
const IOT_EP = process.env.IOT_ENDPOINT;
const IOT_REGION = process.env.REGION;
const dynamodb = new AWS.DynamoDB.DocumentClient();
var iotdataSend;


async function handleReportState(request, context, endpoint) {
	   
    var reportstateSP = endpoint.shadow.state.reported;
    var reportpropertiesSP = reportstateSP[endpoint.modeldescription.nodeID].properties;
    var propertiesprefixSP = endpoint.modeldescription.NodeEndpoint;
    var onoff = reportpropertiesSP[propertiesprefixSP + ":sOnOffS:OnOff"];
    var devname = reportpropertiesSP[propertiesprefixSP+ ":sZDO:DeviceName"];
    console.log('Smart Plug device name: ' + devname);

    var reportdataSP = { };
    var pwrSt;
    var connectivity = 1; 
    var endpointIdSP = endpoint.endpointId;
    var tokenSP = request.directive.endpoint.scope.token;
    var correlationTokenSP = request.directive.header.correlationToken;

    if(onoff == 1)
    {
        reportdataSP[onoff] = "ON";
        pwrSt = "ON";
        console.log('Smart Plug device is ON ');
    }
    else if(onoff == 0)
    {
        reportdataSP[onoff] = "OFF";
        pwrSt = "OFF";
        console.log('Smart Plug device is OFF ');
    }
    
    reportdataSP[connectivity] = "OK";
    var alexaResponseSP = new AlexaResponse(
    {
        "name": 'StateReport',
        "correlationToken": correlationTokenSP,
        "token": tokenSP,
        "endpointId": endpointIdSP
    }
    );

    // Gather current properties and add to our response
    alexaResponseSP.addContextProperty({
        namespace: "Alexa.EndpointHealth",
        name: "connectivity",
        value: {
            value: reportdataSP[connectivity]
        } 
    });
 
    alexaResponseSP.addContextProperty({
        namespace: "Alexa.PowerController",
        name: "powerState",
        value: pwrSt //"ON" //reportdataSP[onoff]
    });

    alexaResponseSP.addContextProperty({
        namespace: "Alexa.BrightnessController",
        name: "brightness",
        value: 50       //TODO: 
    });
    
    return alexaResponseSP.get();
    
}


async function handlePowerControl(request, context, endpoint)
{
    console.log('handlePowerControl....' + request.directive);

    AWS.config.region = IOT_REGION;   //us-west-2, All IoT devices present in this region
    iotdataSend = new AWS.IotData({ endpoint: IOT_EP });

    var endpointId = endpoint.endpointId;
    var thingName = endpoint.thingName;
    var token = request.directive.endpoint.scope.token;
    var correlationToken = request.directive.header.correlationToken;
    var requestMethod = request.directive.header.name; 

    var alexaResponse = new AlexaResponse(
        {
            "correlationToken": correlationToken,
            "token": token,
            "endpointId": endpointId
        }
    );

	    var nodeidSP = endpoint.modeldescription.nodeID;
	    var nodeEP = endpoint.modeldescription.NodeEndpoint;
        var reportstateSP = endpoint.shadow.state.reported;
        var reportpropertiesSP = reportstateSP[nodeidSP].properties;
        var propertiesprefixSP = nodeEP;
        var onoff = reportpropertiesSP[propertiesprefixSP + ":sOnOffS:OnOff"];
        var devname = reportpropertiesSP[propertiesprefixSP+ ":sZDO:DeviceName"];
        console.log('Smart Plug device name: ' + devname);

        let desiredshadowState = {
            state: {
                desired: {
                }
            }
        };
        var desireproperties = {};

        if(requestMethod == 'TurnOn')
        {
            console.log('Smart Plug requested method is TurnOn......');
    		desireproperties[propertiesprefixSP + ":sOnOffS:SetOnOff"] = 1;
            desiredshadowState.state.desired[nodeidSP] = {properties: desireproperties};

            await updateThingShadow(thingName, desiredshadowState);
            
            alexaResponse.addContextProperty({
                namespace: "Alexa.PowerController",
                name: "powerState",
                value: "ON"
            });

            alexaResponse.addContextProperty({
                namespace: "Alexa.BrightnessController",
                name: "brightness",
                value: 50       //TODO: 
            });
    
            return alexaResponse.get();
        }
        else if(requestMethod == 'TurnOff')
        {
            console.log('Smart Plug requested method is TurnOff......');
    		desireproperties[propertiesprefixSP + ":sOnOffS:SetOnOff"] = 0;
            desiredshadowState.state.desired[nodeidSP] = {properties: desireproperties};

            await updateThingShadow(thingName, desiredshadowState);

            alexaResponse.addContextProperty({
                namespace: "Alexa.PowerController",
                name: "powerState",
                value: "OFF"
            });

            alexaResponse.addContextProperty({
                namespace: "Alexa.BrightnessController",
                name: "brightness",
                value: 50       //TODO: 
            });

            return alexaResponse.get();
            
        }
}

async function updateThingShadow(thingName, shadowState) {
    try {
        var payloadjson = JSON.stringify(shadowState)
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        // Updating shadow updates our *control plane*, but it doesn't necessarily
        // mean our device state has changed. The device is responsible for monitoring
        // the device shadow and responding to changes in desired states. 
        const request = iotdataSend.publish(params);
        var updateShadowResponse = await request.promise();
    }
    catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

function getFriendlyName(devname) 
{
    var friendlyName = JSON.parse(devname).deviceName;
    return friendlyName;
}

module.exports = {handleReportState, handlePowerControl, getFriendlyName};
