
const AWS = require("aws-sdk");
const AlexaResponse = require("./AlexaResponse");
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
const IOT_EP = process.env.IOT_ENDPOINT;
const IOT_REGION = process.env.REGION;
const dynamodb = new AWS.DynamoDB.DocumentClient();
var iotdataSend;

async function handleReportState(request, context, endpoint) {

    AWS.config.region = IOT_REGION;   //us-west-2, All IoT devices present in this region hence needed to configure while controlling
    iotdataSend = new AWS.IotData({ endpoint: IOT_EP });

    console.log(endpoint.config);
    var userId = endpoint.config.thingName;
    var devName = endpoint.config.friendlyName;
    var msgId = request.directive.header.messageId;
    
    console.log(request.directive);
    console.log(msgId);

    var endpointId = endpoint.endpointId;
    var reportstate = endpoint.shadow.state.reported;
    var reportproperties = reportstate[endpoint.modeldescription.nodeID].properties;
    var propertiesprefix = endpoint.modeldescription.NodeEndpoint;
    
    var token = request.directive.endpoint.scope.token;
    var correlationToken = request.directive.header.correlationToken;
    var currentState = reportproperties;   
    
    var heatingsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:HeatingSetpoint_x100"];
    var coolingsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:CoolingSetpoint_x100"];
    var systemmode = reportproperties[propertiesprefix + ":sWIFITherS:SystemMode"];
    var scale = reportproperties[propertiesprefix + ":sWIFITherS:TemperatureDisplayMode"];
    var temperature = reportproperties[propertiesprefix + ":sWIFITherS:LocalTemperature_x100"];
    var runningmode = reportproperties[propertiesprefix + ":sWIFITherS:RunningMode"];

    console.log('LocalTemperature_x100: ' + temperature);

    //vinay: 20210602: May need to remove this check incase device goes offline, Alexa will show server unresponsive???
    var connectivity = 1;  //reportstate.connected;
   
    var reportdata = { };
    if (connectivity == 1)  //'true')
        reportdata[connectivity] = "OK";
    else
        reportdata[connectivity] = "FAIL"; // FIXME
        
    //vinay debug
    //reportdata[scale] = "CELSIUS"     //shows in F for Toilet device
    if (reportdata[scale] == "FAHRENHEIT")
    {
        reportdata[temperature] = temperature / 100;
        reportdata[heatingsetpoint] = heatingsetpoint / 100;
        reportdata[coolingsetpoint] = coolingsetpoint / 100;
    }
    else
    {
        reportdata[temperature] = convertTemperature(temperature/100 , "CELSIUS", "FAHRENHEIT");
        reportdata[heatingsetpoint] = convertTemperature(heatingsetpoint/100 , "CELSIUS", "FAHRENHEIT");
        reportdata[coolingsetpoint] = convertTemperature(coolingsetpoint/100 , "CELSIUS", "FAHRENHEIT");
    }

    switch(systemmode) {
        case 0:
            reportdata[systemmode] = "OFF";
            break;
        case 1:
            reportdata[systemmode] = "AUTO";
            break;
        case 2:
            reportdata[systemmode] = "COOL";
            break;  
        case 3:
            reportdata[systemmode] = "COOL";
            break;
        case 4:
            reportdata[systemmode] = "HEAT";
            break; 
    }

    
    // Basic response header
    var alexaResponse = new AlexaResponse(
        {
            "name": 'StateReport',
            "correlationToken": correlationToken,
            "token": token,
            "endpointId": endpointId
        }
    );

    // Gather current properties and add to our response
    alexaResponse.addContextProperty({
        namespace: "Alexa.EndpointHealth",
        name: "connectivity",
        value: {
            value: reportdata[connectivity]
        } 
    });


    if (systemmode == 2) 
    {
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: reportdata[coolingsetpoint],
                //scale: reportdata[scale]
                scale: "FAHRENHEIT"  
            }
        });
    }
    else if ((systemmode == 3) || (systemmode == 4))
    {
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: reportdata[heatingsetpoint],
                //scale: reportdata[scale]
                scale: "FAHRENHEIT"  
            }
        });
    }
    else if (systemmode == 1)
    {
        var autosetpoint = 5;
        if (runningmode == 3)
        {
            autosetpoint = reportdata[coolingsetpoint];
        }
        else if  (runningmode == 4)
        {
            autosetpoint = reportdata[heatingsetpoint];
        }
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: autosetpoint,
                //vinay debug remove later
                scale: "FAHRENHEIT"     //shows in C for Kitchen device
                //scale: reportdata[scale]
            }
        });
    }
        
    alexaResponse.addContextProperty({
        namespace: "Alexa.ThermostatController",
        name: "thermostatMode",
        value: reportdata[systemmode]
    });

    alexaResponse.addContextProperty({
        namespace: "Alexa.TemperatureSensor",
        name: "temperature",
        //vinay
        //value: reportdata[temperature]  
        value: {
                value: reportdata[temperature],
                //scale: reportdata[scale]
                scale: "FAHRENHEIT"
            }
    });
    
    return alexaResponse.get();
}

async function handleThermostatControl(request, context, endpoint) {
    /*  This function handles all requests that Alexa identifies as being a 
        "ThermostatController" directive, such as:
          - Turn device to cool mode
          - Turn device to heat mode
          - Increase device temperature
          - Decrease device temperature
          - Set temperature to X degrees
          
         "Smart Plug" directive, such as:
          - Turn On the lights
          - Turn Off the lights
    */
    console.log('handleST920ThermostatControl....' + request.directive);
    var endpointId = endpoint.endpointId;
    var thingName = endpoint.thingName;
    var token = request.directive.endpoint.scope.token;
    var correlationToken = request.directive.header.correlationToken;
    var requestMethod = request.directive.header.name; 
    var payload = request.directive.payload;
    var msgId = request.directive.header.messageId;

    //vinay
    var errResponse = 0;
    
    var alexaResponse = new AlexaResponse(
        {
            "correlationToken": correlationToken,
            "token": token,
            "endpointId": endpointId
        }
    );

    var modeldescr = await getDeviceType(endpoint);
	console.log(modeldescr.Items[0].thermostatType);
    
    var reportstate = endpoint.shadow.state.reported;
    var propertiesprefix = endpoint.modeldescription.NodeEndpoint;
    var nodeid = endpoint.modeldescription.nodeID;
    var reportproperties = reportstate[endpoint.modeldescription.nodeID].properties;
    var minheatsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:MinHeatSetpoint_x100"];
    var maxheatsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:MaxHeatSetpoint_x100"];
    var mincoolsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:MinCoolSetpoint_x100"];
    var maxcoolsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:MaxCoolSetpoint_x100"];	
    var scale = reportproperties[propertiesprefix + ":sWIFITherS:TemperatureDisplayMode"];
    var runningmode = reportproperties[propertiesprefix + ":sWIFITherS:RunningMode"];
    var systemmode = reportproperties[propertiesprefix + ":sWIFITherS:SystemMode"];
    var heatingsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:HeatingSetpoint_x100"];
    var coolingsetpoint = reportproperties[propertiesprefix + ":sWIFITherS:CoolingSetpoint_x100"];
    
    var desireddata = {};
    var desireproperties = {};
    
    //log(`Running ThermostatControl handler for ${requestMethod} method`);     //vinay

    if (requestMethod === 'SetTargetTemperature') {

        var targetSetpoint = payload.targetSetpoint;
        desireddata.targetsetpoint = 0;
        
        if (payload.hasOwnProperty("targetSetpoint"))
        {
            if (payload.targetSetpoint.scale == "CELSIUS")
            {
        		desireddata.targetsetpoint = targetSetpoint.value * 100;
            }
            else
            {
                desireddata.targetsetpoint = convertTemperature(targetSetpoint.value , "FAHRENHEIT", "CELSIUS") * 100;
            }
        }
    	desireddata.targetsetpoint = Math.round(desireddata.targetsetpoint)

        if ((systemmode == 3) || (systemmode == 1 && runningmode == 3))
        {
    		if(desireddata.targetsetpoint < mincoolsetpoint || desireddata.targetsetpoint > maxcoolsetpoint)
    		{
    		    errResponse = 1;
    	        var errorPayload = {
                validRange: {
                        minimumValue: {
                                value: 10.0,    //50F
                                scale: "CELSIUS"
                                },
                        maximumValue: {
                                value: 37.0,    //99F
                                scale: "CELSIUS"
                        }
                    }
    			};
    			throw new AlexaException(
    				'TEMPERATURE_VALUE_OUT_OF_RANGE',
    				`Requested target setpoint  outside allowed range of ${mincoolsetpoint} to ${maxcoolsetpoint}.`,
    				errorPayload
    			);
    		}
    		
    		desireproperties[propertiesprefix + ":sWIFITherS:SetHeatingSetpoint_x100"] = desireddata.targetsetpoint
    	}
    	else if (((systemmode == 4) || (systemmode == 5)) || (systemmode == 1 && runningmode == 4))
    	{	
    	    console.log('Target Setpoint Validation.....'+ desireddata.targetsetpoint + minheatsetpoint + maxheatsetpoint);
    	    //vinay: change to || not &&
    		if(desireddata.targetsetpoint < minheatsetpoint || desireddata.targetsetpoint > maxheatsetpoint)
    		{
    		    errResponse = 1;
        	    console.log('Target Setpoint Validation Started........'+ errResponse);
    	        var errorPayload = {
                validRange: {
                        minimumValue: {
                                value: 5.0,     //41F
                                scale: "CELSIUS"
                                },
                        maximumValue: {
                                value: 32.0,    //90F
                                scale: "CELSIUS"
                        }
                    }
    			};
    			throw new AlexaException(
    				'TEMPERATURE_VALUE_OUT_OF_RANGE',
    				`Requested target setpoint outside allowed range of ${minheatsetpoint} to ${maxheatsetpoint}.`,
    				errorPayload
    			);
    		}
    		desireproperties[propertiesprefix + ":sWIFITherS:SetHeatingSetpoint_x100"] = desireddata.targetsetpoint;
    	}
	
        let desiredshadowState = {
            state: {
                desired: {
                }
            }
        };
        desiredshadowState.state.desired[nodeid] = {properties: desireproperties};

        //console.log(desiredshadowState)   //vinay

        // For debugging purposes, we may choose to copy the desired state to reported state:
        //if (copyDesiredStateToReportedStateInShadow === true) {
        //    shadowState.state.reported = shadowState.state.desired;
        //}
        // FIXME: Should we ge shadow after get the data?

        await updateThingShadow(thingName, desiredshadowState);

        //vinay, alexa response after successful update
        if(errResponse == 0)
        {
        var targetpointContextProperty = {
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: {
                value: targetSetpoint.value,
                scale: targetSetpoint.scale
            }
        };
        alexaResponse.addContextProperty(targetpointContextProperty);
        return alexaResponse.get();
        }
        
    }
    else if (requestMethod === 'AdjustTargetTemperature') {
//        var currentSetpoint = endpoint.shadow.state.reported.targetSetpoint;
        var currentSetpoint = 5;
        if ((systemmode == 2)  || (systemmode == 1 && runningmode == 3))
            currentSetpoint = coolingsetpoint / 100;
        else if (((systemmode == 3) || (systemmode == 4))  || (systemmode == 1 && runningmode == 4))
            currentSetpoint = heatingsetpoint / 100
        
        var currentValue = currentSetpoint;
        var currentScale = "CELSIUS";
        
        var targetSetpointDelta = payload.targetSetpointDelta;
        var deltaValue = targetSetpointDelta.value;
        var deltaScale = targetSetpointDelta.scale;

        //log('Current setpoint:', currentSetpoint);    //vinay
        //log('Target delta:', targetSetpointDelta);    //vinay

        // It's possible that the requested temperature change is in a different
        // scale from that being used/reported by the device. In such a case, 
        // the Alexa guide states we should report the new adjusted value in the
        // device's scale. When the scales are different, we must convert the
        // current temperature to the scale of the requested delta in order to
        // add (or subtract) the delta from the current temperature to arrive
        // at the new desired temperature. Then, we must convert this new value
        // back to the temp scale currently in use by the device. If the current
        // and delta scales are the same (e.g. both Fahrenheit or both Celsius),
        // then the convertTemperature() function simply returns the same value
        // it was provided, without modification. 
        // https://developer.amazon.com/docs/device-apis/alexa-thermostatcontroller.html#adjusttargettemperature-directive
        var currentValueInDeltaScale = convertTemperature(currentValue, currentScale, deltaScale);
        var newValueInDeltaScale = currentValueInDeltaScale + deltaValue;
        var newValueInCurrentScale = convertTemperature(newValueInDeltaScale, deltaScale, currentScale);

        newValueInCurrentScale = Math.round(newValueInCurrentScale)
        var newTargetSetpoint = {
            value: newValueInCurrentScale,
            scale: currentScale
        };

        if ((systemmode == 3)  || (systemmode == 1 && runningmode == 3))
            desireproperties[propertiesprefix + ":sWIFITherS:SetHeatingSetpoint_x100"] = newValueInCurrentScale * 100;
        else if (((systemmode == 4) || (systemmode == 5))  || (systemmode == 1 && runningmode == 4))
            desireproperties[propertiesprefix + ":sWIFITherS:SetHeatingSetpoint_x100"] = newValueInCurrentScale * 100;

        let desiredshadowState = {
            state: {
                desired: {
                    
                }
            }
        };
        desiredshadowState.state.desired[nodeid] = {properties: desireproperties};
        
        // For debugging purposes, we may choose to copy the desired state to reported state:
//        if (copyDesiredStateToReportedStateInShadow === true) {
//            shadowState.state.reported = shadowState.state.desired;
//        }
// FIXME: Should we get shadow after get the data?

        await updateThingShadow(thingName, desiredshadowState);

        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "targetSetpoint",
            value: newTargetSetpoint
        });

        return alexaResponse.get();

    }
    else if (requestMethod === 'SetThermostatMode') {
        
        var thermostatMode = payload.thermostatMode;
        var targetmode = 0;
        switch (thermostatMode.value)
        {
            case "OFF":
                targetmode = 0;
                break;
            case "AUTO":
                targetmode = 1;
                break;
            case "COOL":
                targetmode = 3;
                break;
            case "HEAT":
                targetmode = 4;
                break;
            default:
                targetmode = 0;
                break;
                
        }

        desireproperties[propertiesprefix + ":sWIFITherS:SetSystemMode"] = targetmode
        let desiredshadowState = {
            state: {
                desired: {
                    
                }
            }
        };
        desiredshadowState.state.desired[nodeid] = {properties: desireproperties};


        // For debugging purposes, we may choose to copy the desired state to reported state:
//        if (copyDesiredStateToReportedStateInShadow === true) {
//            shadowState.state.reported = shadowState.state.desired;
//        }
        // FIXME: Should we get shadow after get the data?

        await updateThingShadow(thingName, desiredshadowState);

        // Context properties are how we affirmatively tell Alexa that the state
        // of our device after we have successfully completed the requested changes.
        // The not required, it is recommended that *all* properties be reported back, 
        // regardless of whether they were changed. 
        // At the moment, we are only reporting back the changed property.
        alexaResponse.addContextProperty({
            namespace: "Alexa.ThermostatController",
            name: "thermostatMode",
            value: thermostatMode.value
        });

        return alexaResponse.get();
    }
    else {
        log(`ERROR: Unsupported request method ${requestMethod} for ThermostatController.`);
        throw new AlexaException(
            'INTERNAL_ERROR',
            'Unsupported request method ${requestMethod} for ThermostatController'
        );
    } 

}

async function updateThingShadow(thingName, shadowState) {
    try {
        var payloadjson = JSON.stringify(shadowState)
        var sendtopic = "$aws/things/" + thingName + "/shadow/update"
        var params = {
            payload: Buffer.from(payloadjson),
            topic: sendtopic,
            qos: '1'
        };
        // Updating shadow updates our *control plane*, but it doesn't necessarily
        // mean our device state has changed. The device is responsible for monitoring
        // the device shadow and responding to changes in desired states. 
        const request = iotdataSend.publish(params);
        var updateShadowResponse = await request.promise();
    }
    catch (err) {
        console.log('Error: unable to update device shadow:', err);
        throw (err);
    }
}

async function getDeviceType(endpoint)
{
    var dev = endpoint.endpointId
	var modelId = dev.split("-")[2]
    var params = {
        TableName: MODEL_TABLE_NAME,
	    KeyConditionExpression: 'ModelID = :ModelID',
		ExpressionAttributeValues: {
		':ModelID' : modelId
		}
	}
    var modeldesc = await dynamodb.query(params).promise();
    return modeldesc; 
}

function convertTemperature(temperature, currentScale, desiredScale) {
    if (currentScale === desiredScale) {
        return temperature;
    }
    else if (currentScale === 'FAHRENHEIT' && desiredScale === 'CELSIUS') {
        return convertFahrenheitToCelsius(temperature);
    }
    else if (currentScale === 'CELSIUS' && desiredScale === 'FAHRENHEIT') {
        return convertCelsiusToFahrenheit(temperature);
    }
    else {
        throw new Error(`Unable to convert ${currentScale} to ${desiredScale}, unsupported temp scale.`);
    }
    
}

function convertCelsiusToFahrenheit(celsius) {
    var fahrenheit = Math.round(((celsius * 1.8) + 32) * 100);
    log(`Converted temperature to ${fahrenheit} FAHRENHEIT.`);
    return fahrenheit/100;
}

function convertFahrenheitToCelsius(fahrenheit) {
    var celsius = Math.round(((fahrenheit - 32) * 0.556)*100);
    //log(`Converted temperature to ${celsius} CELSIUS.`);  //vinay
    return celsius / 100;
}

function log(message1, message2) {
    if (message2 == null) {
        console.log(message1);
    } else {
        console.log(message1 + JSON.stringify(message2, null, 2));
    }
}

function AlexaException(name, message, additionalPayload) {
    log('Creating handled Alexa exception...');
    // The error name should be one of the Alexa.ErrorResponse interface types:
    // https://developer.amazon.com/docs/device-apis/alexa-errorresponse.html
    var error = new Error(); 
    this.stack = error.stack;
    this.name = name;
    this.message = message;
    this.AlexaError = true;
    this.additionalPayload = checkValue(additionalPayload, undefined);
}

function checkValue(value, defaultValue) {

    if (value === undefined || value === {} || value === "") {
        return defaultValue;
    }
    return value;
}

function getFriendlyName(devname) 
{
    return devname;
}

module.exports = {handleReportState, handleThermostatControl, convertCelsiusToFahrenheit, convertFahrenheitToCelsius, getFriendlyName};

