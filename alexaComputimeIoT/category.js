//TEST NUMBER 6
async function handleCategoryReportState(modelCategory, request, context, endpoint)
{
    let response;

    var CtgLib = modelCategory.Items[0].categoryLib;
    response = await require(CtgLib).handleReportState(request, context, endpoint);

    return response;
}

async function handleCategoryThermostatControl(modelCategory, request, context, endpoint)
{
    let response;

    var CtgLib = modelCategory.Items[0].categoryLib;
    response = await require(CtgLib).handleThermostatControl(request, context, endpoint);

    return response;
}

async function handleCategoryPowerControl(modelCategory, request, context, endpoint)
{
    let response;

    var CtgLib = modelCategory.Items[0].categoryLib;
    response = await require(CtgLib).handlePowerControl(request, context, endpoint);

    return response;
}

async function handleCategoryPowerLevelControl(modelCategory, request, context, endpoint)
{
    let response;

    var CtgLib = modelCategory.Items[0].categoryLib;
    response = await require(CtgLib).handlePowerLevelControl(request, context, endpoint);

    return response;
}

function handleFriendlyName(modelCategory, devname)
{
    var CtgLib = modelCategory.Items[0].categoryLib;
    var friendlyName = require(CtgLib).getFriendlyName(devname);
    return friendlyName;
}

module.exports = {handleCategoryReportState, handleCategoryThermostatControl, handleFriendlyName, handleCategoryPowerControl, handleCategoryPowerLevelControl};
