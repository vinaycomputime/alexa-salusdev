const AWS = require("aws-sdk");

const GET_DEVICES_BY_USER_FUNCTION = process.env.GET_DEVICES_BY_USER_FUNCTION;
const VERIFY_COGNITO_TOKEN_FUNCTION = process.env.VERIFY_COGNITO_TOKEN_FUNCTION;
const IOT_ENDPOINT = process.env.IOT_ENDPOINT;
const DEVICE_TABLE_NAME = process.env.DEVICE_TABLE;
const MODEL_TABLE_NAME = process.env.MODEL_TABLE;
const IOT_REGION = process.env.REGION;

const AlexaResponse = require("./AlexaResponse");
const CtgFxn = require("./category");
var iot;
var iotdata;
const dynamodb = new AWS.DynamoDB.DocumentClient();
const lambda = new AWS.Lambda();
const REQUIRED_PAYLOAD_VERSION = "3";
//vinay
var accessToken;
var global_debug = 0;

/*
    TODO: 
    
    1. Consider re-writing to store endpoint-to-user mapping as an attribute
       in IoT registry rather than DynamoDB; eliminate need for DDB? 

    2. Consider storing attributes needed for discovery, e.g. manufacturer name, 
       as attributes in IoT core, instead of a Lambda config file? 
         - on second thought, the complex nested structure of things like the
           Alexa capabilities means simple IoT attributes probably wouldn't
           suffice. DynamoDB comes to mind, but it's easier to store this info
           in a config file rather than an item in a DDB database. So, maybe
           makes sense to keep it where it is (in discoveryConfig.js)
*/

exports.handler = async function (request, context) {

    try {
        
        AWS.config.region = IOT_REGION;
        
        iot = new AWS.Iot();
        iotdata = new AWS.IotData({ endpoint: IOT_ENDPOINT });
        
        console.log("alexaSalusIoT....\n");

        verifyRequestContainsDirective(request);
        
        var directive = request.directive;
        var header = directive.header;
        var namespace = header.namespace; 
        var name = header.name;
        var ignoreExpiredToken = request.ignoreExpiredToken || false;   // True for debugging purposes, if we're using an old token

        verifyPayloadVersionIsSupported(header.payloadVersion);
        
        var userId = await verifyAuthTokenAndGetUserId(
            namespace,
            directive,
            ignoreExpiredToken
        );

        console.log(request.directive); //vinay
            
        // Is this a request to discover available devices?
        //vinay: debug
        //namespace = 'Alexa.Discovery'
        if (namespace === 'Alexa.Discovery') {
            var response = await handleDiscovery(request, context, userId);
            return sendResponse(response.get());
        }
        else if (namespace === 'Alexa.Authorization') {
            var response = await handleAuthorization(request, context, userId);
            return sendResponse(response.get());
        }
        // If this is not a discovery request, it is a directive to do something
        // to or retrieve info about a specific endpoint. We must verify that
        // the endpoint exists * and * is mapped to our current user before 
        // we do anything with it: 
        else {
           
            var endpoint = await verifyEndpointAndGetEndpointDetail(userId, directive.endpoint.endpointId);
    
            verifyEndpointConnectivity(endpoint);

            var modeldescription = await getDeviceType(endpoint);
	        console.log(modeldescription.Items[0].thermostatType);

            if (namespace === 'Alexa.ThermostatController') {
                let response = await CtgFxn.handleCategoryThermostatControl(modeldescription, request, context, endpoint);
                return sendResponse(response.get());
            }
            else if (namespace === 'Alexa.PowerController') {
                let response = await CtgFxn.handleCategoryPowerControl(modeldescription, request, context, endpoint);
                return sendResponse(response.get());
            }
            else if (namespace === 'Alexa.PowerLevelController') {
                let response = await CtgFxn.handleCategoryPowerLevelControl(modeldescription, request, context, endpoint);
                return sendResponse(response.get());
            }
            else if (namespace === 'Alexa' && name === 'ReportState') {
                let response = await handleReportState(request, context, endpoint);
                return sendResponse(response.get());
            }
            else {       
                throw new AlexaException(
                    'INVALID_DIRECTIVE', 
                    `Namespace ${namespace} with name ${name} is unsupported by this skill.`
                );
            }
        }
    }
    catch (err) {
        log(`Error: ${err.name}: ${err.message}`);
        log('Stack:\n' + err.stack);
        var errorType, errorMessage, additionalPayload;

        // if AlexaError key is present (regardless of value), then we threw
        // an error intentionally and we want to bubble up a specific Alexa
        // error type. If the key is not present, it is an unhandled error and
        // we respond with a generic Alexa INTERNAL_ERROR message.
        if (err.hasOwnProperty('AlexaError')) {
            errorType = err.name;
            errorMessage = err.message;
            additionalPayload = checkValue(err.additionalPayload, undefined);
        } else {
            errorType = 'INTERNAL_ERROR';
            errorMessage = `Unhandled error: ${err}`
        }
        return sendErrorResponse(errorType, errorMessage, additionalPayload);
    }

};

function verifyEndpointConnectivity(endpoint) {

    var shadow = endpoint.shadow;

    if (shadow.hasOwnProperty('state') === false) {
        throw new AlexaException('ENDPOINT_UNREACHABLE', 'Shadow does not contain a state object');
    }

    if (shadow.state.hasOwnProperty('reported') === false) {
        throw new AlexaException('ENDPOINT_UNREACHABLE', 'Shadow does not contain a state.reported object');
    }
    
    if (shadow.state.reported.hasOwnProperty('connected') === false) {
        throw new AlexaException('ENDPOINT_UNREACHABLE', 'Shadow does not contain a state.reported.connectivity object');
    }
    //vinay: 20210602: May need to remove this check incase device goes offline, Alexa will show server unresponsive???
    //if (shadow.state.reported.connected !== 'true') {
    //    throw new AlexaException('ENDPOINT_UNREACHABLE', `Device unavailable, reported online=${shadow.state.reported.connected}`);
    //}

}

function verifyRequestContainsDirective(request) {
    // Basic error checking to confirm request matches expected format.
    if (!('directive' in request)) {
        throw new AlexaException(
            'INVALID_DIRECTIVE',
            'directive is missing from request'
        );
    }
}

function verifyPayloadVersionIsSupported(payloadVersion) {
    // Basic error checking to confirm request matches expected format.
    if (payloadVersion !== REQUIRED_PAYLOAD_VERSION) {
        throw new AlexaException(
            'INVALID_DIRECTIVE', 
            `Payload version ${payloadVersion} unsupported.`
            + `Expected v${REQUIRED_PAYLOAD_VERSION}`
        );
    }
}

async function verifyEndpointAndGetEndpointDetail(userId, endpointId) {
    /*
    If a directive (other than discovery) is received for a specific endpoint, 
    there are two ways the endpoint may be invalid: Alexa Cloud may think it is
    mapped to our specific user but it has since been reassigned to another user, 
    or, the mapping is correct but, for whatever reason, the "Thing" in AWS IoT
    no longer exists. In the former, an example scenario is that the device was
    given to a friend and that friend registered it to their account. The Alexa
    Cloud does not know this happened... we do not want the original owner to be
    able to control the device at this point, so we should be sure to verify the
    device is associated to the user invoking the skill. In the latter example, 
    it wouldn't be typical to delete an IoT Thing for a given device but its not
    impossible (maybe by mistake, or maybe because a device was suspected as being
    compromised and we want to remove it from service?).
    */
    
    // First, let's see if device is mapped to our user in DynamoDB:
    //log('Verifying that endpoint is mapped to the user invoking the skill...')  //vinay
    /*
    var params = {
        Key: {
            hashId: 'userId_' + userId,        
            sortId: 'thingName_' + endpointId
        }, 
        TableName: DEVICE_TABLE_NAME
    };
    log('Calling dynamodb.getItem() with params:', params);
    var getResponse = await dynamodb.get(params).promise();
    */
    console.log('user ID.....' + userId);
    var params = {
        ExpressionAttributeValues: {
            ":userid": userId
        }, 
        ProjectionAttributes: "userid",  
        KeyConditionExpression: "userid = :userid", 
        TableName: DEVICE_TABLE_NAME
    };
  
    var query_response = await dynamodb.query(params).promise();

    var ownlist = (JSON.parse(query_response.Items[0].Own));
    var sharelist = (JSON.parse(query_response.Items[0].Own));
    var devlist = ownlist.list;
    devlist.concat(sharelist.list);
    
    if (devlist.indexOf(endpointId) > -1) {
        // The Item key will only be present if the item exists.
        //log("Endpoint is mapped to invoking user.");
    }
    else {
        //SMART PLUG: DynamoDB content is just gateway, endpointId is the actual device ID, It doesn't make sense to compare/verify.
        /*throw new AlexaException(
            'NO_SUCH_ENDPOINT',
            'Endpoint ID exists in AWS IoT Registry but is not mapped to this user in DynamoDB'
        );*/
    }

    // Now, let's see if the device actually exists in the Iot Core registry.
    // If it does exist, we will populate our response with the IoT thing details
    // and current reported shadow state. 
    var endpoint = {};
    var modelType;
    try {
        // The IoT describeThing() API will throw an error if the given thing
        // name does not exist, so we must wrap in a try/catch block.
        console.log('Verifying existence of endpoint in AWS IoT Registry...');
        let params = {
            thingName: endpointId
        };
//        log('Calling iot.describeThing() with params:', params);
        endpoint = await iot.describeThing(params).promise();
//        log("Endpoint exists in Iot Registry");
//        endpoint.config = getDeviceConfigFromIotThingShadow(endpoint.attributes);
        
        // Endpoint ID is core concept when dealing with the Alexa Smart Home API;
        // We happen to use the IoT Registry's thingName as our endpointID, but
        // its possible other implementations may use a different value for the
        // endpoint ID. So, rather than let all later code refer to "thingName",
        // I'd rather explicitly create an endpointId key. That way, it's easier
        // to drop in a different value if you prefer not to use the IoT thing name.
        endpoint.endpointId = endpoint.thingName;
        //20220124:
        modelType = endpoint.thingName.split("-");
        if(modelType.length > 2)
        endpoint.modeldescription = await getModelDescriptionbyModelID(endpoint.thingName.split("-")[2]);    //Smart Plug/iT600 -> [2]
        else
        endpoint.modeldescription = await getModelDescriptionbyModelID(endpoint.thingName.split("-")[0]);    //Solo    

        endpoint.shadow = await getDeviceShadow(endpoint.thingName);
        endpoint.config = await getDeviceConfigFromIotThingShadow(endpoint);
    }
    catch (err) {
        if (err.name === "ResourceNotFoundException") {
            throw new AlexaException(
                'NO_SUCH_ENDPOINT',
                'Endpoint ID does not exist as Thing Name in AWS IoT Registry'
            );
        }
        else {
            // If it's not a ResourceNotFound error, then it is an unexpected
            // error and we simply pass it upstream to our main error handler.
            throw(err); 
        }
    }
    return endpoint;
}

async function handleDiscovery(request, context, userId) {

    try {
        log("Calling handleDiscovery()");
    
        var alexaResponse = new AlexaResponse({
            "namespace": "Alexa.Discovery",
            "name": "Discover.Response"
        });

        var endpoints = await getUserEndpoints(userId);

        endpoints.forEach(endpoint => {
            alexaResponse.addPayloadEndpoint(endpoint);
        });

        return alexaResponse;
    }
    catch (err) {
        throw new Error("handleDiscovery() failed: " + err);
    }

}

async function handleAuthorization(request, context, userId) {

    try {
        log("Calling handleAuthorization()");
    
        var alexaResponse = new AlexaResponse({
            "namespace": "Alexa.Authorization",
            "name": "ErrorResponse"
        });
        
        return alexaResponse;
    }
    catch (err) {
        throw new Error("handleAuthorization() failed: " + err);
    }

}

async function verifyAuthTokenAndGetUserId(namespace, directive, ignoreExpiredToken) {
    /* This function calls another function to determine whether the auth token 
       provided by Alexa during invocation is valid and not expired.  
    */
    
    try {
        //log('Validating auth token...');    //vinay
        // The location of the user's auth token is in the payload of the 
        // request directive if the namespace is Discovery; otherwise, it is
        // in the request endpoint:
        var encodedAuthToken;
        //vinay
        var user;
        
        if (namespace === 'Alexa.Discovery') {
            encodedAuthToken = directive.payload.scope.token;
            accessToken = directive.payload.scope.token;
        }
        else if (namespace === 'Alexa.Authorization') {
            encodedAuthToken = directive.payload.grantee.token;
        }
        else {
            encodedAuthToken = directive.endpoint.scope.token;
        }


        var params = {
            FunctionName: VERIFY_COGNITO_TOKEN_FUNCTION, 
            InvocationType: "RequestResponse", 
            Payload: JSON.stringify({ token: encodedAuthToken })
        };
    
        var response = await lambda.invoke(params).promise();

        if (response.hasOwnProperty('FunctionError')) {
            var authError = JSON.parse(response.Payload);
            log('authError: ', authError)
            // Jason
            if (authError.errorMessage === 'Token is expired' ) {
//                 && ignoreExpiredToken === true) {
                throw new AlexaException(
                    'EXPIRED_AUTHORIZATION_CREDENTIAL',
                    'Auth token is expired'
                );
            }
            else {
                throw new AlexaException(
                    'INVALID_AUTHORIZATION_CREDENTIAL',
                    'Auth token is invalid....'
                );
            }
        }
        //console.log('Auth token is valid...');    //vinay
        var plaintextAuthToken = JSON.parse(response.Payload);
        //vinay
        //console.log('Lambda Invoke response.....'+ plaintextAuthToken.username);  //vinay
        
        // Cognito has both a username and a sub; a sub is unique and never
        // reasigned; a username can be reasigned; it is therefore important
        // to use the 'sub' and not the username as the User ID:
//        var userId = plaintextAuthToken.sub;
        var userId = getUserIdbyUserName(plaintextAuthToken.username);        
        console.log(userId);

        if(global_debug == 1)
        userId = 'us-west-2:0416e5a6-a4a8-437c-b54a-0b1fc6d9da50';

        return userId;
    }
    catch (err) {
        console.log(`Error invoking ${VERIFY_COGNITO_TOKEN_FUNCTION}: ${err}`);
        throw (err);
    }
}

// Jason
async function getUserIdbyUserName(userName) {
	var user = userName;
    //Fetch info from DynamoDB
	var params = {
		  TableName: "UserToDeviceList",
		  IndexName: "UserName-index",
		  KeyConditionExpression: 'UserName = :username',
		  ExpressionAttributeValues: {
	   	  ':username' : user
		  }
	   }
   var userdata = await dynamodb.query(params).promise();
   //console.log(userdata);     //vinay
    var userid = userdata.Items[0].userid;
    //console.log('Received User ID....'+ userid);  //vinay
    return userid;
}

async function getUserEndpoints(userId) {
    /*
    This function takes a user ID obtained from the validated and not-expired auth
    token provided by Alexa in the function request and invokes another function
    that returns a list of all devices associated to that user. 
    */
    
    var payload = JSON.stringify({
        userId: userId
    });

    var params = {
        FunctionName: GET_DEVICES_BY_USER_FUNCTION, 
        InvocationType: "RequestResponse", 
        Payload: payload
    };

    var getUserDevicesResponse = await lambda.invoke(params).promise();

    var devices = (JSON.parse(getUserDevicesResponse.Payload)).deviceList;

    var endpoints = [];

    for (const device of devices) {
        
        let params = {
            thingName: device.thingName
        };

        var iotDescription = await iot.describeThing(params).promise();
        iotDescription.endpointId = device.thingName;
        iotDescription.modeldescription = device;
        iotDescription.shadow = await getDeviceShadow(device.thingName);
        var thingConfig = await getDeviceConfigFromIotThingShadow(iotDescription);

        var endpoint = {
            endpointId: device.thingName,
            manufacturerName: thingConfig.manufacturerName,
            friendlyName: thingConfig.friendlyName,
            description: thingConfig.description,
            displayCategories: thingConfig.displayCategories,
            capabilities: thingConfig.capabilities,
        };

        endpoints.push(endpoint);
    }
    
    return endpoints;
}

async function getModelDescriptionbyModelID(modelID) {
	var model = modelID;
    //Fetch info from DynamoDB
	var params = {
		  TableName: MODEL_TABLE_NAME,
		  KeyConditionExpression: 'ModelID = :ModelID',
		  ExpressionAttributeValues: {
	   	  ':ModelID' : model
		  }
	   }
   var modeldescription = await dynamodb.query(params).promise();
   console.log(modeldescription.Items[0]);

   return modeldescription.Items[0];
}

//Since using await inside hence changed func to async
async function getDeviceConfigFromIotThingShadow(attributes) {
    
    var result = {};

    var reportstate = attributes.shadow.state.reported;
    console.log('getDeviceConfigFromIotThingShadow...1' + attributes.modeldescription.nodeID)
    var reportproperties = reportstate[attributes.modeldescription.nodeID].properties;
    var propertiesprefix = attributes.modeldescription.NodeEndpoint;
    result.thingName = attributes.thingName;
    result.manufacturerName = "Salus";
    var modeldescr = await getDeviceType(attributes);
	console.log('getDeviceConfigFromIotThingShadow.... ' + modeldescr.Items[0]);
	var devname;

	devname = modeldescr.Items[0].deviceName;
	devname = reportproperties[propertiesprefix + devname];
	result.friendlyName = CtgFxn.handleFriendlyName(modeldescr, devname);

    result.description = attributes.modeldescription.deviceType;
    result.displayCategories = attributes.modeldescription.AlexaDisplayCategories;
    result.capabilities = attributes.modeldescription.AlexaCapabilities;

    return result;
}

function log(message1, message2) {
    if (message2 == null) {
        console.log(message1);
    } else {
        console.log(message1 + JSON.stringify(message2, null, 2));
    }
}

async function getDeviceType(endpoint)
{
    var dev = endpoint.endpointId;
    var modelId;
    
    //20220124:
    var devType = dev.split("-");
    if(devType.length > 2)
	modelId = devType[2];
    else
	modelId = devType[0];

    var params = {
        TableName: MODEL_TABLE_NAME,
	    KeyConditionExpression: 'ModelID = :ModelID',
		ExpressionAttributeValues: {
		':ModelID' : modelId
		}
	}
    var modeldesc = await dynamodb.query(params).promise();
    return modeldesc; 
}

async function handleReportState(request, context, endpoint) {
    
    log('Gathering state from IoT Thing shadow to report back to Alexa...');
    
    //SMART PLUG
    //var dev = 'SAU2AG1_GW-001E5E00830A-SP600-001E5E090215F1A7'
	var response;

    var modeldescription = await getDeviceType(endpoint);
	console.log(modeldescription.Items[0]);
	console.log(modeldescription.Items[0].thermostatType);
	
	response = await CtgFxn.handleCategoryReportState(modeldescription, request, context, endpoint);
	return response.get();
}


async function getDeviceShadow(thingName) {
    // Get the device's reported state per the state.reported object of the
    // corresponding IoT thing's device shadow.
    var params = {
        thingName: thingName
    };

    var response = await iotdata.getThingShadow(params).promise();
    var shadow = JSON.parse(response.payload);
    log('getThingShadow() response:', shadow);
    return shadow;
    
}

function sendResponse(response) {
    return response
}

function sendErrorResponse(type, message, additionalPayload) {
    log("Preparing error response to Alexa Cloud...");
    var payload = {
        "type": type,
        "message": message
    };
    // Certain Alexa error response types allow or require additional parameters in the payload response
    if (additionalPayload !== undefined) {
        payload = { ...payload, ...additionalPayload }
    }
    var alexaErrorResponse = new AlexaResponse({
        "name": "ErrorResponse",
        "payload": payload
    });
    return sendResponse(alexaErrorResponse.get());
}

function AlexaException(name, message, additionalPayload) {
    log('Creating handled Alexa exception...');
    // The error name should be one of the Alexa.ErrorResponse interface types:
    // https://developer.amazon.com/docs/device-apis/alexa-errorresponse.html
    var error = new Error(); 
    this.stack = error.stack;
    this.name = name;
    this.message = message;
    this.AlexaError = true;
    this.additionalPayload = checkValue(additionalPayload, undefined);
}

function checkValue(value, defaultValue) {

    if (value === undefined || value === {} || value === "") {
        return defaultValue;
    }
    return value;
}